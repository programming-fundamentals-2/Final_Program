package jLottery;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.*;

public class jLottery implements ActionListener {
	//create objects and vars
	static JFrame frame = new JFrame("The Lottery Program");
	static Container con = frame.getContentPane();
	Random RandomNum = new Random();
	
	int check_six=0;
	int matches = 0;
	int[] UserEntries = new int[6];
	int[] WinningNums = winningNUM();
	
	//Create Window
	public void CreateFrame (){
		frame.setSize(1200,800);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setAlwaysOnTop(true);
		frame.setResizable(false);
	}
	
	//Set Up Content Container
	public void CreateContainer(){
		con.setBackground(new Color(56,48,44));
		con.setLayout(new GridLayout(0,5));
	}
	
	//Add a Label
	public void addHead(String i) {
	    JLabel label = new JLabel(i);
	    label.setFont(new Font("Sans-Serif", Font.BOLD, 20));
	    label.setForeground(new Color(225,98,25));
	    con.add(label);
	}
	
	public void addSubHead(String i) {
	    JLabel label = new JLabel(i);
	    label.setFont(new Font("Sans-Serif", Font.PLAIN, 20));
	    label.setForeground(new Color(225,98,25));
	    con.add(label);
	}
	
	 
	//function to create check boxes and add them to frame ( complete with action listeners )
	public void addCheckButton(int i){
		String ButtonNum = Integer.toString(i);
		JCheckBox select = new JCheckBox(ButtonNum);
		select.setBackground(new Color(56,48,44));
		select.setForeground(new Color(225,255,255));
		
		con.add(select);
		select.addActionListener(this);	
		select.setActionCommand(Integer.toString(i));
	}

	public void addButton(String i){
		JButton button = new JButton(i);
		button.setBackground(new Color(225, 98, 25));
		button.setForeground(new Color(255, 255, 255));
		button.setFont(new Font("Sans-Serif", Font.BOLD, 20));

		con.add(button);
		button.addActionListener(this);
	}
	
	
	//function to create array of 6 random numbers ( return int[] array = new int[6]{rand(),rand(),rand(),rand(),rand(),rand()} )
	private int[] winningNUM () { 
		int[] win = new int[6];
			for (int i=0; i<6; i++){
				win[i]= RandomNum.nextInt(35) ;
			}
		return win;
		}  
	
	
	//function to compare created lottery numbers with user's numbers ( return int = num of matching numbers )
	private int getMatches(){
		int[] userNums = this.UserEntries;
		int[] winningNums = this.WinningNums;
		
		for (int x : userNums) {
			   for (int y : winningNums) {
			      if (x == y) { this.matches+=1;}
			   }
		}
		return this.matches;
	}
	
	//Writer method for displaying the winning numbers
	public void PrintWinningNums(){
		int[] WinningNums = this.WinningNums;
		for (int i=0;i<WinningNums.length ; i++){ 
			System.out.println(Integer.toString(WinningNums[i]));
		}
	}
	
	//function to display prize/winning message based on number of matching numbers (static 'writer' method, switch statement)
	public void PrintResults(){
		int n = this.getMatches();
		switch (n){
		case 0 : JOptionPane.showMessageDialog(frame,
			    "You Lose!\n0 Numbers");
		break;
		case 1 : //intentional fall-through 
		case 2 :JOptionPane.showMessageDialog(frame,
			    "You Win!\n1-2 Numbers\n$5"); 
		break;
		case 3 :JOptionPane.showMessageDialog(frame,
			    "You Win!\n3 Numbers\n$250"); 
		break;
		case 4 :JOptionPane.showMessageDialog(frame,
			    "You Win!\n4 Numbers\n$1,000"); 
		break;
		case 5 :JOptionPane.showMessageDialog(frame,
			    "You Win!\n5 Numbers\n$250,000"); 
		break;
		case 6 :JOptionPane.showMessageDialog(frame,
			    "Jackpot!\n6 Numbers\n$1,000,000"); 
		break;
		
		}
	}
	
	//THIS RUNS WHENEVER AN ACTION IS PERFORMED!
	public void actionPerformed(ActionEvent evt)  { 	
			
			//String Action= evt.paramString();
			this.check_six++;
			if (this.check_six<6){ 
				this.UserEntries[this.check_six]= Integer.parseInt(evt.getActionCommand()); //get button value
			}
            else {
            	con.setVisible(false); //hide our main container.
            	JOptionPane.showMessageDialog(frame, "ENTERED: " + 
            			Integer.toString(this.UserEntries[0])+" "+
            			Integer.toString(this.UserEntries[1])+" "+
            			Integer.toString(this.UserEntries[2])+" "+
            			Integer.toString(this.UserEntries[3])+" "+
            			Integer.toString(this.UserEntries[4])+" "+
            			Integer.toString(this.UserEntries[5])+" \n"+
            			"WINNING: " + 
            			Integer.toString(this.WinningNums[0])+" "+
            			Integer.toString(this.WinningNums[1])+" "+
            			Integer.toString(this.WinningNums[2])+" "+
            			Integer.toString(this.WinningNums[3])+" "+
            			Integer.toString(this.WinningNums[4])+" "+
            			Integer.toString(this.WinningNums[5])+" \n"
            			);
            	
            	this.PrintResults();
            	frame.dispose(); 
            }
			
       }
	
	
}
