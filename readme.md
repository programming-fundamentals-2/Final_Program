# Final Program

**Student:** Liam Hockley

**Submitted Date:** Apr 27, 2012 11:14 am

**Grade:** 75.0 (max 75.0)

**Instructions**
> There will be two paths to the final program.
> 
> Challenge (Full 75 pts)
> You will create a very simple lottery program that provides a GUI that contains 31 checkboxes for user selection of numbers from 0 - 30 simulating the selection of lotto numbers.  You will give them the ability to check 6 and only six boxes.  Once their numbers are selected - you will compare them with six new randomly generated numbers.  You will then ( in a text box ) let them know how many of their numbers matched the six lotto numbers and how much money they won.
> 
> Award them 0, 1,  or 2 correct numbers - -$0
> 3 correct numbers -- $100
> 4 correct numbers -- $10000
> 5 correct numbers -- $1000000
> 
> The attached document - lotto.docx provides an couple of my screen shots.
> 
> or
> 
> Less Challenging Path ( B - 60 pts)
>  
> You will develop a GUI stopwatch - it will contain two JButtons > Start and Stop and a JLabel that states -- Stopwatch is running .... 
> Then when the stop button is pressed it will have calculated the > elapsed time and prints out:
> Elapsed time is 1.778 
> 
> You will need to use the Calender Class and the following methods:
> getInstance()
> getTimeInMillis()
> to calculate the time from the start button pressed to the stop button pressed.